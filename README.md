# Conexión a base de datos con patron singleton

## Participantes
1. **Hafid Oxte Dzul** 
    * ***Matricula***: 140300042
    * ***gitlab***: <https://gitlab.com/HafidOD>
    
2. **Oscar Ricardo Yama Martin** 
    * ***Matricula***: 150300804
    * ***gitlab***: <https://gitlab.com/RicardoYamartin>

3. **Nardi Suribel Santamaria Rejon**
    * ***Matricula***: 150300130
    * ***gitlab***: <https://gitlab.com/nardisuribel>

4. **Brian Asael Noh Cocom**
    * ***Matricula***: 130300126
    * ***gitlab***: <https://gitlab.com/asaelNC>

5. **Julio Cesar Palacios Torres**
    * ***Matricula***: 150300156
    * ***gitlab***: <https://gitlab.com/JulioPalacios97>

6. **Luis Manuel Vargas Guerrero**
    * ***Matricula***: 140300057
    * ***gitlab***: <https://gitlab.com/LMVGUERRERO>
___

Clonar repositorio en su maquina local, posteriormente ingresar a la carpeta que se creo.

Ejecutar el siguiente comando para instalar las dependencias necesarias para ejecutar el proyecto.
<pre> npm install </pre>

Para iniciar el servidor 
<pre> npm run dev </pre>

Para ver desde el navegador solo engresa la URL:
<http://localhost:3000/>
