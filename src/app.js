'use strict'

const express = require('express');
const app = express();
const indexRouter = require('./routes/IndexRoute');
const mongoRouter = require('./routes/MongoRoute');
const sqlServerRouter = require('./routes/SQLServerRoute');
const mysql = require('./routes/Mysql-Route');
const couchDBRouter = require('./routes/CouchDBRoute');

app.use(express.json());

//Routes
app.use(indexRouter);
app.use(mongoRouter);
app.use(sqlServerRouter);
app.use(mysql);
app.use(couchDBRouter);
//app.use('/api', router);
 
module.exports = app;
