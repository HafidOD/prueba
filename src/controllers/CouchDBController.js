'use strict'
// const couchDB = require('./../database/SingletonCouchDB');

module.exports = {
    // test: function(req, res){
    //     couchDB.listDatabases().then((dbs) => {
    //         res.status(200).json({
    //             'message': 'Test CouchDB',
    //             'method': 'GET',
    //             'data': dbs
    //         })
    //     });
    // },

    get: function(req, res){
        res.status(200).json({
            'message': 'Fetched CouchDB',
            'method': 'GET',
            'data': 'json.data'
        });
    },

    create: function(req, res){
        res.status(200).json({
            'message': 'Created CouchDB',
            'method': 'POST',
            'data': req.body
        })
    },

    update: function(req, res){
        res.status(200).json({
            'message': 'Updated CouchDB',
            'method': 'PUT',
            'data': req.body
        })
    },

    delete: function(req, res){
        res.status(200).json({
            'message': 'Deleted CouchDB',
            'method': 'DELETE',
            'data': req.body
        })
    },
}