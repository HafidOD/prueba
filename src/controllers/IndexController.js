'use strict'

function index (req, res){
    res.status(200).json({'message': 'Index'});
}

module.exports = {
    index
}