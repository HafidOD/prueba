'use strict'

module.exports = {
    get: function(req, res){
        res.status(200).json({
            'message': 'Fetched',
            'method': 'GET',
            'data': 'json.data'
        });
    },

    create: function(req, res){
        res.status(200).json({
            'message': 'Created',
            'method': 'POST',
            'data': req.body
        })
    },

    update: function(req, res){
        res.status(200).json({
            'message': 'Updated',
            'method': 'PUT',
            'data': req.body
        })
    },

    delete: function(req, res){
        res.status(200).json({
            'message': 'Deleted',
            'method': 'DELETE',
            'data': req.body
        })
    },

}
