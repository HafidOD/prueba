'use strict'

const mysql = require('mysql');

const mysqlConnection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'julioc97',
    database: 'company'
});

mysqlConnection.connect(function (err){
   if(err){
       console.log(err);
       return;
   } else {
       console.log('DB connected'); 
   }
});

module.exports = mysqlConnection;
