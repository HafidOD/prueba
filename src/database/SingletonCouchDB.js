'use strict'

const NodeCouchDB = require('node-couchdb');

const couch =  new NodeCouchDB({
  auth: {
    user:     process.env.COUCHDB_USER,
    password: process.env.COUCHDB_PASSWORD,
  }
});

module.exports = couch;