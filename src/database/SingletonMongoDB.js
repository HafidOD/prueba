'use strict'

const mongoose = require('mongoose');
const URI = 'mongodb://localhost/mdb';

mongoose.connect(URI, {
  useCreateIndex: true,
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true
})
  .then(db => console.log('Db Connected'))
  .catch(err => console.error(err));

module.exports = mongoose;

/*
var Db = require('mongodb').Db;
var Connection = require('mongodb').Connection;
var Server = require('mongodb').Server;
//the MongoDB connection
var connectionInstance;

module.exports = function(callback) {
  //if already we have a connection, don't connect to database again
  if (connectionInstance) {
    callback(connectionInstance);
    return;
  }

  var db = new Db('your-db', new Server("127.0.0.1", Connection.DEFAULT_PORT, { auto_reconnect: true }));
  db.open(function(error, databaseConnection) {
    if (error) throw new Error(error);
    connectionInstance = databaseConnection;
    callback(databaseConnection);
  });
};
*/

/*
module.exports = function(callback){
    if (connectionInstance == '' || connectionInstance == null){
      callback(connectionInstance);
      return;
    }

    mongoose.connect(URI, {
      useCreateIndex: true,
      useNewUrlParser: true,
      useFindAndModify: false,
      useUnifiedTopology: true
    })
      .then( db => {
        db = connectionInstance;
        console.log(db);
        console.log('db connected');
        callback(db);
      })
      .catch(err=> console.error(err));

  };
*/