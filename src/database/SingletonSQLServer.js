'use strict'

var Connection = require('tedious').Connection;
var config = {
    server: 'DESKTOP-I2287MH\\SQLEXPRESS',
    authentication: {
        type: 'default',
        options: {
            userName: 'DESKTOP-I2287MH\\Ricardo',
            password: ''
        }
    },
    options: {
        database: 'prueba',
        instanceName: 'Sqlexpress',
        rowCollectionOnDone: true,
        useColumnNames: false
    }
}
var connection = new Connection(config);
connection.on('connect', function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log('Connected');
    }
});
module.exports = connection;


