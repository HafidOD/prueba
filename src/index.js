'use strict'

// Establecer env variables
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
require('dotenv').config({
  path: `${__dirname}/environments/.env.${process.env.NODE_ENV}`
});

// require('./database/uu');
require('./database/SingletonMongoDB');
require('./database/SingletonSQLServer');
require('./database/Singleton-Mysql');
require('./database/SingletonCouchDB');

const app = require('./app');

// Set port
app.set('port', process.env.PORT || 3000);

// Starting server
app.listen(app.get('port'), () => {
    console.log('Server on port:', app.get('port'));
}); 
 