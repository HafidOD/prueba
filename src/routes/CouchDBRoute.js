'use strict'

const router = require('express').Router();
const couchDBController = require('../controllers/CouchDBController');

// router CouchDB
router.get('/CouchDB', couchDBController.get);
router.post('/CouchDB', couchDBController.create);
router.put('/CouchDB', couchDBController.update);
router.delete('/CouchDB', couchDBController.delete);
// router.get('/CouchDB/test', couchDBController.test);

module.exports = router;