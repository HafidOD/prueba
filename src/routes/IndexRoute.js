'use strict'

const router = require('express').Router();
const  indexController = require('../controllers/IndexController');

// router index
router.get('/', indexController.index);

module.exports = router;