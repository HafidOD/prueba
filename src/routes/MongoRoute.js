'use strict'

const router = require('express').Router();
const mongoController = require('../controllers/MongoController');

// router mongodb
router.get('/mongodb', mongoController.get);
router.post('/mongodb', mongoController.create);
router.put('/mongodb', mongoController.update);
router.delete('/mongodb', mongoController.delete);

module.exports = router;