'use strict'

const router = require('express').Router();
const msyqlController = require('../controllers/Mysql-Controller');

// router mysqldb
router.get('/mysql_db', msyqlController.get);
router.post('/mysql_db', msyqlController.create);
router.put('/mysql_db', msyqlController.update);
router.delete('/mysql_db', msyqlController.delete);

module.exports = router;
