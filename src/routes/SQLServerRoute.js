'use strict'

const router = require('express').Router();
const sqlServerController = require('../controllers/SQLServerController');

// router SqlServer
router.get('/sqlserver', sqlServerController.get);
router.post('/sqlserver', sqlServerController.create);
router.put('/sqlserver', sqlServerController.update);
router.delete('/sqlserver', sqlServerController.delete);

module.exports = router;