const router = require('express').Router();
const productosController = require('../controllers/sqliteController');
router.get('/', sqliteController.read);

router.get('/sqlite', sqliteController.read);
router.post('/sqlite', sqliteController.insertData);
router.delete('/sqlite', sqliteController.deleteData);


module.exports = router;
